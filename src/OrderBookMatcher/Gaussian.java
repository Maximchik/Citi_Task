package OrderBookMatcher;

public class Gaussian {
	// these two are used to generate Gaussian distribution. Since they are const 
	// they were declared as static final
	private static final double epsilon = Double.MIN_VALUE;
	private static final double two_pi = 2.0*3.14159265358979323846;
	
	// These are used in Gaussian
	private double z1 = 0;
    private boolean generate = false;
	
	// sample source code was taken from WIKI page 
	// https://en.wikipedia.org/wiki/Box-Muller_transform
	// and converted to Java from C++
	public double generateGaussianNoise(double mu, double sigma) {	
		generate = !generate;

        if (!generate)
           return z1 * sigma + mu;

        double u1, u2;
        do {
           u1 = Math.random();
           u2 = Math.random();
         } while ( u1 <= epsilon );

        double z0;
        z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(two_pi * u2);
        z1 = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(two_pi * u2);
        return z0 * sigma + mu;
	}
}
