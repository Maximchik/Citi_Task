package OrderBookMatcher;

import java.util.*;

public class OrderQueue {

	private int totalQ = 0;
	int accumulatedQ=0;
	
	List<Order> queue = new ArrayList<Order>();
	
	public OrderQueue() {
		
	}
	
	public int getTotalQ() { return totalQ; }
	
	public void AddOrder(Order ord) {
		queue.add(ord);
		totalQ += ord.remainingQ;
	}
	
	public int Count() {
		return queue.size();
	}
}