package OrderBookMatcher;

import java.io.FileInputStream;
import java.util.*;

public class Application extends TimerTask {
	boolean matching = false;
	private OrderBook book;
	private Gaussian gauss;
	private DummyLog log;

	final String SETTINGS_FILE_NAME = "Citi.xml";

	// Order size mean and std
    private double SizeMean = 100000;
    private double SizeSTD = 20000;
    
    // Order price mean and std
    private double PriceMean = 50;
    private double PriceSTD = 6;
    
    private double tick_size = 1.0;
	
    private long speedUp = 1000;
    
    private long OrderDelaySecs = 30; // 30 secs delay
    private int StrategyActivationTimeMin = 15; // number of calls (15 minutes * twice a minute)
    private int called_counter = 0;
	
	Timer sendOrderTimer = new Timer(); // this is a recurrent timer, fired every "DELAY" secs (30 in this case)
	
	public Application() {
		book = new OrderBook(tick_size);
		gauss = new Gaussian();
		log = new DummyLog("log.txt"); 
		LoadConfig();
		
		sendOrderTimer.schedule(this, 10, OrderDelaySecs);
	}
	
	private void LoadConfig() {
	    Properties properties = new Properties();
	    try 
	    {
		    FileInputStream fis = new FileInputStream(SETTINGS_FILE_NAME);
		    properties.loadFromXML(fis);
		    SizeMean = Double.parseDouble(properties.getProperty("SizeMean", "100000"));
		    SizeSTD = Double.parseDouble(properties.getProperty("SizeSTD", "20000"));
		    PriceMean = Double.parseDouble(properties.getProperty("PriceMean", "50"));
		    PriceSTD = Double.parseDouble(properties.getProperty("PriceSTD", "6"));

		    tick_size = Double.parseDouble(properties.getProperty("tick_size", "1.0"));

		    speedUp = Long.parseLong(properties.getProperty("SpeedUp", "1000"));
		    OrderDelaySecs = Long.parseLong(properties.getProperty("OrderDelaySecs", "30"));
		    
		    StrategyActivationTimeMin = Integer.parseInt(properties.getProperty("StrategyActivationTimeMin", "15"));
		    
		    log.println("Using the following inital values for this run...");
		    String values = "SizeMean=" + SizeMean + ", SizeSTD="+ SizeSTD + ", PriceMean=" + PriceMean + ", PriceSTD=" + PriceSTD + 
		    				"\nTickSize=" + tick_size + ", OrderDelaySecs=" + OrderDelaySecs + ", StrategyActivationTimeMin=" + StrategyActivationTimeMin;
		    System.out.println(values);
		    log.println(values);
		    StrategyActivationTimeMin *= 2;
		    OrderDelaySecs *= speedUp;
	    }
	    catch (Exception ex)
	    {
	    	System.err.println("Could not load configuration file " + SETTINGS_FILE_NAME + " exception " + ex.getMessage());
	    }

	}
	
	private void SendOrder() {
		int Q = (int)gauss.generateGaussianNoise(SizeMean, SizeSTD);
		double price = (int)gauss.generateGaussianNoise(PriceMean, PriceSTD);
		
		Order.Side side = Order.Side.SELL;
		if ( Math.random() >= 0.5)
			side = Order.Side.BUY;
			
		Order ord = new Order("12345.HK", price, Q, side);
		book.Add(ord);
		
		log.println("Sending order " + ord.toString() + " to order book");
	}
	
	private void ActivateStrategy() {
		log.println("Current state of the order book");
		book.calcCumulativeQnty();
		log.println(book.toString());
		log.println("Activating the strategy...");
		Estimates est = book.GetOpeningEstimates();
		if ( (est.Price > 0) && (est.Volume > 0) ) {
			log.print("opens @" + est.Price + " volume=" + est.Volume + " ");
			if ( est.OpensOnBid )
				log.print("on buy side");
			else
				log.print("on sell side");
			log.println(", remaining volume " + est.Imbalance);
		}
		else 
			log.println("Error: both price level and volume are zero, volume maximization strategy can not determine the opening volume!");
	}
	
	
	public void run() {
		// generate order Q
		if ( called_counter++ < StrategyActivationTimeMin) 
			SendOrder();
		else { 
			ActivateStrategy();
			sendOrderTimer.cancel();
			log.close();
		}
	}
}
