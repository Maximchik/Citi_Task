package OrderBookMatcher;

public class Order {
	public enum Side { BUY, SELL };

	private static long currentOrderID = 0;
	
	String symbol; 
	double price;
	int Q;
	int remainingQ;
	Side side;
	private long orderID;
	
	public Order(String sym, double p, int q, Side s) {
		symbol = sym;
		price = p;
		side = s;
		remainingQ = Q = q;
		orderID = currentOrderID++;
	}
	
	public long GetOrderID() { return orderID; }
	
	public String toString()
	{
		return side.toString() + " order for " + symbol + " " + Q + "@" + price + ", orderID=" + orderID;   
	}
}
