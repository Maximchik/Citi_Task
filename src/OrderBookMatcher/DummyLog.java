package OrderBookMatcher;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Date;
import java.sql.Timestamp;

public class DummyLog {
	private PrintWriter log;
	private String buffer = "";

	public DummyLog(String fileName) {
		try {
			log = new PrintWriter(new FileWriter(fileName));
		}
		catch (IOException ex)
		{
			System.out.println("Coould not create " + fileName + " log file!");
			ex.printStackTrace();
		}
	}
	
	public void println(String str) {
		Date date = new Date();
		if (buffer.isEmpty())
			log.println(new Timestamp(date.getTime()) + ": " + str);
		else {
			log.println( buffer + str);
			buffer = "";
		}
		log.flush();
	}
	
	public void print(String str) {
		Date date = new Date();
		if ( buffer.isEmpty() )
			buffer = new Timestamp(date.getTime()) + ": " + str;
		else 
			buffer += str;
	}

	public void close()
	{
		log.close();
	}
}
