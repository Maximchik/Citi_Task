package OrderBookMatcher;

import java.util.*;

public class OrderBook {
	private String symbol;
	private double tick_size;
	
	private Map<Double, OrderQueue> buys = new TreeMap<>(
			new Comparator<Double>() {
				@Override
				public int compare(Double lhs, Double rhs) {
					return lhs.compareTo(rhs);
				}
			});
	private Map<Double, OrderQueue> sells = new TreeMap<Double, OrderQueue>();
	
	public OrderBook(double tick) {
		symbol = "12345.HK";
		tick_size = tick;
	}
	
	public void Add(Order ord) {
		OrderQueue queue;
		if ( ord.side == Order.Side.BUY ) {
			if (!buys.containsKey(ord.price)) {					
				queue = new OrderQueue();
				buys.put(ord.price, queue);
			}
			else { 
				queue = buys.get(ord.price);
			}
			queue.AddOrder(ord); 
		}
		else if ( ord.side == Order.Side.SELL ) {
			if (!sells.containsKey(ord.price)) {					
				queue = new OrderQueue();
				sells.put(ord.price, queue);
			}
			else { 
				queue = sells.get(ord.price);
			}
			queue.AddOrder(ord); 
		}
	}
	
	public Estimates GetOpeningEstimates() {
		Estimates est = new Estimates();
		
		double sells_min_price = (double)sells.keySet().toArray()[0];
		double sells_max_price = (double)sells.keySet().toArray()[sells.size()-1];

		double buys_min_price = (double)buys.keySet().toArray()[0];
		double buys_max_price = (double)buys.keySet().toArray()[buys.size()-1];


		double min_price = Math.min(sells_min_price, buys_min_price);
		double max_price = Math.max(sells_max_price, buys_max_price);
		
		for(Double price = min_price; price <= max_price; price+= tick_size) {
			int SellVolume = 0;
			int BuyVolume = 0;
			int OpenVolume = 0;
			if ( sells.containsKey(price) || buys.containsKey(price)) {
				if ( sells.containsKey(price) )
					SellVolume = sells.get(price).accumulatedQ;
				if ( buys.containsKey(price) )
					BuyVolume = buys.get(price).accumulatedQ;
				
				OpenVolume = Math.min(SellVolume, BuyVolume);
				if ( (est.Volume <= OpenVolume) && (OpenVolume > 0) ) {
					est.Volume = OpenVolume;
					est.Price = price;
					if ( SellVolume > BuyVolume) {
						est.OpensOnBid = true;
						est.Imbalance = SellVolume - BuyVolume;
					}
					else {
						est.OpensOnBid = false;
						est.Imbalance = BuyVolume - SellVolume;
					}
				}
			}
		}
		
		return est;
	}
	
	public void calcCumulativeQnty() {
//		if ( (sells.size() == 0) || (buys.size() == 0) ) {
//			System.err.println("At least one side of the order book is empty, can not do volume maximazation!");
//			return;
//		}
		
		double sells_min_price = 0;
		double sells_max_price = 0;

		double buys_min_price = 0;
		double buys_max_price = 0;

		if ( !sells.isEmpty()) {
			sells_min_price = (double)sells.keySet().toArray()[0];
			sells_max_price = (double)sells.keySet().toArray()[sells.size()-1];
		}
		
		if ( !buys.isEmpty()) {
			buys_min_price = (double)buys.keySet().toArray()[0];
			buys_max_price = (double)buys.keySet().toArray()[buys.size()-1];
		}

		double min_price = Math.min(sells_min_price, buys_min_price);
		double max_price = Math.max(sells_max_price, buys_max_price);
		
		int prevQ = 0;
		for(Double price = min_price; price <= max_price; price+= tick_size) {
			if (sells.containsKey(price)) {
				sells.get(price).accumulatedQ = prevQ + sells.get(price).getTotalQ();
				prevQ = sells.get(price).accumulatedQ;
			}
			else { // calculate accumulated Q 
				OrderQueue q = new OrderQueue();
				q.accumulatedQ = prevQ;
				sells.put(price, q);
			}
		}

		prevQ = 0;
		for(Double price = max_price; price >= min_price; price-= tick_size) {
			if (buys.containsKey(price)) {
				buys.get(price).accumulatedQ = prevQ + buys.get(price).getTotalQ();
				prevQ = buys.get(price).accumulatedQ;
			}
			else { // calculate accumulated Q
				OrderQueue q = new OrderQueue();
				q.accumulatedQ = prevQ;
				buys.put(price, q);
			}
		}
	}
	
	public String customFormat(String frmt, int value) {
		return String.format(frmt, value);
	}
	
	public String toString() {
		String return_str = "\n      Sells           |     Buys\nN   cumQ    Q       price     Q    cumQ   N\n";
		
		double sells_min_price = 0;
		double sells_max_price = 0;

		double buys_min_price = 0;
		double buys_max_price = 0;

		if ( !sells.isEmpty()) {
			sells_min_price = (double)sells.keySet().toArray()[0];
			sells_max_price = (double)sells.keySet().toArray()[sells.size()-1];
		}

		if ( !buys.isEmpty()) {
			buys_min_price = (double)buys.keySet().toArray()[0];
			buys_max_price = (double)buys.keySet().toArray()[buys.size()-1];
		}

		double min_price = Math.min(sells_min_price, buys_min_price);
		double max_price = Math.max(sells_max_price, buys_max_price);
		
		for(Double price = max_price; price >= min_price; price-= tick_size) {
			if (sells.containsKey(price)) {
				return_str += sells.get(price).Count() + " " + customFormat("% 6d", sells.get(price).accumulatedQ) + " " + customFormat("% 6d", sells.get(price).getTotalQ()) + "  ";
			}
			else 
				return_str += "                      ";

			return_str += price + "  ";

			if ( buys.containsKey(price) ) {
				return_str += customFormat("% 6d", buys.get(price).getTotalQ()) + " " + customFormat("% 6d", buys.get(price).accumulatedQ) + "  " + buys.get(price).Count();
			}
			else 
				return_str += "             ";

			return_str += "\n";
		}
		return return_str;
	}
}