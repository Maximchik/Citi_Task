To Run:
Run RunMe file under Linux or copy starting command alog with the jar file and Citi.xml file.

To speed execution (for testing purposes) edit speedUp parameter to make it smaller, make it 10 from 1000 and the program will run 10 times faster...

Assumptions:
For simplicity it was assumed that price tick size is 1 to avoid creating many empty price levels.
It was assumed that in the case below:

count cumQ Q price Q  cumQ count

1      95  5  101  0   90    0

0      90  0  100  40  90    1

1      90  90  99  10  50    1

max volume would be @ 100, as there are people willing to sell @99 and people willing to buy @100, even though there are actually no orders on 100 level.

classes:
OrderBook - class consists of sorted map that contains order level and a PriceQueue class. Sorted map was chosen for simplicity, though non-sorted could also be used for performance reasons, for example.

PriceQueue - class wraps a price level for either BUY or SELL side. It uses ArrayList as a container. This class calculates the total quatity on the level. "accumulatedQ" for the level is made public and is calculated from the OrderBook class. This is done for possible pefrormance reasons. Since accumulated Qs are needed only once to calculate execution volume, they are calculated right before the strategy run, not at insertion of every order.

Order - class describes order, declares enum order side. A very simple and small class than later could be extended. For simplicity reason it has an open interface, exposing almost all of its memebers.

Gaussian - is the class that generates normal distribution. The algorithm was taken from https://en.wikipedia.org/wiki/Box-Muller_transform and converted to Java from C++.

A small Java program that is sampling 10000 numbers from the distribution and writing them in a file was created for verification. (see "normal_dstribution_price.txt" and "normal_dstribution_q.txt" files). A tiny python script "test.py" was used to load the data file and print mean and std for verification. Note that you will need scipy, numpy and pandas python libs to have it running.

Estimation is a small class that carries all the information about order book, projected volume, side on which the market will execute the order and volume disbalace for possible future use.

DummyLog is a small log class that inserts a time stamp for each logged entry, allowing to verify the order sending and strategy activation schedules.

Application is the main program that creates order book, schedules order creation timer and activates the strategy. For simplicity strategy is activated after a counter of order sending passes a 30 level (15 minutes, twice a minute).

Main is the main class that starts the application.

Algorithm:
Application starts, loading all its default values from config file Citi.xml. Then application creates a log file called log.txt. Then it schedules order creation for the specified period of time. Once an order is created and is inserted into an order book it is recorded into the log with the time stamp of order creation.

After a specified time a volume estimation algorithm starts. It calcualtes cummulative quantities of the order book, writes order book state into a log, with Sell side orders on the left and buy side orders on the right. Order Book is written from high price down to the lower price.

The volume maximization strategy processes order book and writes its predictions into log file, specifying price level, expected volume and opening side of the order book.

Test cases:
Test cases are described in TestCases.txt file.
